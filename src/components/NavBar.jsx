import { useContext } from "react";
import { GlobalContext } from "../context/GlobalContext";
import ThemeToggler from "./ThemeToggler";

const NavBar = () => {
  const { theme } = useContext(GlobalContext);
  console.log(theme);

  return (
    <div className="w-full">
      <div className="flex items-center w-full py-2 justify-between">
        <h1 className="text-white text-3xl font-bold">Tdo</h1>
      </div>
    </div>
  );
};
export default NavBar;
