import { useContext, useEffect, useState } from "react";
import { TodoContext } from "../context/TodoContext";
import { addTodo, generateRandom } from "../utils/functions";
import AddTodo from "./AddTodo";
import Todo from "./Todo";

const TodoList = () => {
  const { todos } = useContext(TodoContext);

  const filteredTodos = todos
    .sort((a, b) => new Date(b.now) - new Date(a.now))
    .sort((a, b) => a.completed - b.completed);

  return (
    <div>
      <AddTodo />
      {filteredTodos.map((todo) => (
        <Todo key={todo.id} todo={todo} />
      ))}
    </div>
  );
};
export default TodoList;
