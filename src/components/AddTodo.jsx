import { useContext, useEffect, useState } from "react";
import { TodoContext } from "../context/TodoContext";
import { generateRandom } from "../utils/functions";
import dayjs from "dayjs";
import { notify } from "../utils/functions";
import { ToastContainer } from "react-toastify";

const AddTodo = () => {
  const { todos, setTodos } = useContext(TodoContext);
  const [title, setTitle] = useState("");

  const onHandleSubmit = (e) => {
    e.preventDefault();
    if (title === "") {
      return;
    } else {
      const id = generateRandom();
      const date = new Date();
      const now = dayjs(date).format();

      const todo = { id, title, now, completed: false };

      setTodos([...todos, todo]);
      setTitle("");
      notify(`Todo added ${title}.`);
    }
  };

  return (
    <div className="flex items-center justify-center my-8">
      <ToastContainer />
      <form onSubmit={onHandleSubmit} className="my-2">
        <input
          type="text"
          placeholder="Add New Todo"
          value={title}
          onChange={(e) => setTitle(e.target.value)}
          className="bg-white p-2 px-3 outline-none border-none text-lg rounded mx-2"
        />
        <button className="bg-white text-lg px-3 py-2 rounded">Submit</button>
      </form>
    </div>
  );
};
export default AddTodo;
