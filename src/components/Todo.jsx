import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
import { MdDelete } from "react-icons/md";
import { TodoContext } from "../context/TodoContext";
import { useContext } from "react";
import { notify } from "../utils/functions";
import { ToastContainer } from "react-toastify";

const Todo = ({ todo }) => {
  const { todos, setTodos } = useContext(TodoContext);
  dayjs.extend(relativeTime);
  const { title, completed, now, id } = todo;

  const handleCompleted = () => {
    setTodos(
      todos.map((todo) => {
        if (todo.id === id) {
          return { ...todo, completed: !completed };
        } else {
          return todo;
        }
      })
    );
    notify(`Todo Updated ${title} ${completed ? "completed" : ""}`);
  };
  const deleteTodo = () => {
    setTodos(todos.filter((a) => a.id !== todo.id));
    notify(`Todo Deleted ${title}`);
  };
  return (
    <>
      <ToastContainer />
      <div className="flex justify-between items-center  bg-zinc-800 p-3 text-white">
        <div className="flex gap-5 items-center">
          <input
            type="checkbox"
            checked={completed}
            onChange={handleCompleted}
          />
          <h3
            className={`text-xl font-semibold ${completed && "line-through"}`}
          >
            {title}
          </h3>
        </div>
        <div className="flex gap-3 items-center">
          <span className="text-zinc-400">{dayjs().fromNow(dayjs(now))}</span>
          <span
            onClick={deleteTodo}
            className="cursor-pointer p-1 rounded-full hover:bg-zinc-700 transition"
          >
            <MdDelete />
          </span>
        </div>
      </div>
    </>
  );
};
export default Todo;
