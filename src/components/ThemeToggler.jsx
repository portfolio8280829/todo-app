import { useContext, useState } from "react";
import { GlobalContext } from "../context/GlobalContext";
import { FaMoon, FaSun } from "react-icons/fa";

const ThemeToggler = () => {
  const { theme, toggleTheme } = useContext(GlobalContext);

  return (
    <div className="">
      {theme === "dark" ? (
        <button className="text-white" onClick={() => toggleTheme()}>
          <FaMoon />
        </button>
      ) : (
        <button className="text-white" onClick={() => toggleTheme()}>
          <FaSun />
        </button>
      )}
    </div>
  );
};
export default ThemeToggler;
