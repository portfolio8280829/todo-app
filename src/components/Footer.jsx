const Footer = () => {
  return (
    <div className="text-white flex justify-between items-center fixed bottom-0 p-2 max-w-6xl mx-auto w-full">
      <div className="text-zinc-400 text-sm">Tdo</div>
      <div className="text-zinc-400 text-sm">All Rights Reserved - 2024</div>
    </div>
  );
};
export default Footer;
