import { createContext, useEffect, useMemo, useState } from "react";

export const TodoContext = createContext(null);

export const TodoContextProvider = ({ children }) => {
  const [todos, setTodos] = useState(
    JSON.parse(localStorage.getItem("todos") || "[]")
  );

  useEffect(() => {
    localStorage.setItem("todos", JSON.stringify(todos));
  }, [todos]);

  const value = useMemo(() => ({ todos, setTodos }), [todos]);

  console.log(value);

  return <TodoContext.Provider value={value}>{children}</TodoContext.Provider>;
};
