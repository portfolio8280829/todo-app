import { toast } from "react-toastify";

export const generateRandom = () => {
  return Date.now() + Math.floor(Math.random() * 1e4);
};

export const notify = (message) =>
  toast(message, {
    position: "top-center",
    autoClose: 1250,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: false,
    draggable: false,
    progress: undefined,
    theme: "dark",
  });
