import NavBar from "./components/NavBar";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { TodoContextProvider } from "./context/TodoContext";
import TodoList from "./components/TodoList";
import Footer from "./components/Footer";

function App() {
  return (
    <div className="bg-zinc-900 text-black w-full h-screen p-3">
      <div className="max-w-6xl mx-auto">
        <NavBar />
        <TodoContextProvider>
          <TodoList />
        </TodoContextProvider>
        <Footer />
      </div>
    </div>
  );
}

export default App;
